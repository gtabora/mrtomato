﻿using System;
using System.Collections.Generic;
using System.Linq;
using AuthProject.Areas.Identity.Data;
using AuthProject.Models;

namespace AuthProject.AppServices
{
    public class CategoriesAppService
    {
        private readonly AuthProjectIdentityDbContext _context;

        public CategoriesAppService(AuthProjectIdentityDbContext context)
        {
            _context = context;
        }

        public List<CategoryViewModel> GetCategories()
        {
            List<Category> categories = _context.Category.ToList();

            if (!categories.Any()) return new List<CategoryViewModel>();

            return categories.Select(x => new CategoryViewModel
            {
                Id=x.CategoryId,
                Description=x.Description,
                RolId=x.RoleId,
                Roles=new List<RolesViewModel>()
            }).ToList();
        }

        public List<RolesViewModel> GetRoles()
        {
            var roles = _context.Roles.ToList();

            if (!roles.Any()) return new List<RolesViewModel>();

            return roles.Select(x => new RolesViewModel
            {
                RolId = x.Id,
                Description = x.NormalizedName
            }).ToList();
        }

        public CategoryViewModel GetCategory(int id)
        {
            var roles = GetRoles();

            if (id == 0)
            {
                return new CategoryViewModel
                {
                    Roles = roles,
                    RolId=string.Empty
                };
            }

            Category category = _context.Category.FirstOrDefault(x => x.CategoryId == id);

            if (category == null) return new CategoryViewModel();

            return new CategoryViewModel
            {
                Id = category.CategoryId,
                Description = category.Description,
                RolId = category.RoleId,
                Roles = roles
            };
        }

        public void Save(CategoryViewModel model)
        {
            if (model != null)
            {
                if (model.Id == 0)
                {
                    Create(model);
                }
                else
                {
                    Update(model);
                }
            }          
        }

        public void Create(CategoryViewModel model)
        {
            Category newCategory = new Category.Builder(model.Description, model.RolId).Build();

            _context.Category.Add(newCategory);

            _context.SaveChanges();

        }

        public void Update(CategoryViewModel model)
        {
            Category category = _context.Category.FirstOrDefault(x => x.CategoryId == model.Id);

            category.Description = model.Description;
            category.RoleId = model.RolId;

            _context.Update(category);

            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            Category category = _context.Category.FirstOrDefault(x => x.CategoryId == id);

            if (category != null)
            {
                _context.Remove(category);

                _context.SaveChanges();
            }
        }
    }
}
