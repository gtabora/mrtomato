﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AuthProject.Areas.Identity.Data;
using AuthProject.Models;
using Microsoft.AspNetCore.Identity;

namespace AuthProject.AppServices
{
    public class UserAppService
    {
        private readonly AuthProjectIdentityDbContext _context;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;

        public UserAppService(AuthProjectIdentityDbContext context, UserManager<IdentityUser> userManager, RoleManager<IdentityRole> roleManager)
        {
            _context = context;
            _userManager = userManager;
            _roleManager = roleManager;
        }

        public async Task<List<UserViewModel>> GetUsers()
        {
            var users = _context.Users.ToList();

            if (!users.Any()) return new List<UserViewModel>();

            var usersWithRole = new List<UserViewModel>();

            foreach(var x in users)
            {
                var user = new UserViewModel
                {
                    Email = x.Email,
                    RoleId = await GetRole(x),
                    Id=x.Id
                };

                usersWithRole.Add(user);
            }

            return usersWithRole;
        }

        public async Task<string> GetRole(IdentityUser user)
        {
            var roles = await  _userManager.GetRolesAsync(user);

            if (roles.Any()) return roles.First();

            return string.Empty;
        }

        public List<RolesViewModel> GetRoles()
        {
            var roles = _context.Roles.ToList();

            if (!roles.Any()) return new List<RolesViewModel>();

            return roles.Select(x => new RolesViewModel
            {
                RolId = x.NormalizedName,
                Description = x.NormalizedName
            }).ToList();
        }

        public async Task<UserViewModel> GetUser(string id)
        {
            var roles = GetRoles();

            if (string.IsNullOrEmpty(id))
            {
                return new UserViewModel
                {
                    Roles = roles,
                    RoleId = string.Empty
                };
            }

            IdentityUser user = _context.Users.FirstOrDefault(x => x.Id == id);

            if (user == null) return new UserViewModel();

            return new UserViewModel
            {
                Id = user.Id,
                Email = user.Email,
                RoleId = await GetRole(user),
                Roles = roles
            };
        }

        public async Task Save(UserViewModel model)
        {
            if (model != null)
            {
                await Update(model);
            }
        }

        public async Task Update(UserViewModel model)
        {
            IdentityUser user = _context.Users.FirstOrDefault(x => x.Id == model.Id);

            await RemoveRoles(user).ConfigureAwait(false);

            await SaveNewRole(user, model.RoleId);
        }

        public async Task SaveNewRole(IdentityUser user, string newRole)
        {
            await _userManager.AddToRoleAsync(user, newRole).ConfigureAwait(false);
        }

        public async Task RemoveRoles(IdentityUser user)
        {
            var roles = await _userManager.GetRolesAsync(user);

            await _userManager.RemoveFromRolesAsync(user, roles);
        }


        public void Delete(int id)
        {
            Category category = _context.Category.FirstOrDefault(x => x.CategoryId == id);

            if (category != null)
            {
                _context.Remove(category);

                _context.SaveChanges();
            }
        }
    }
}
