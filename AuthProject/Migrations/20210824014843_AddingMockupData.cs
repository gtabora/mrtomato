﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AuthProject.Migrations
{
    public partial class AddingMockupData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "8f797b11-8271-4cc7-9525-3a93ef87f06b", "8ae811cc-c33b-4dbc-9f0d-a783ee7f015c", "Admin", "Admin" });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "c69caafa-1014-4423-9db9-fab7d376619e", "04335528-d854-429c-af0c-f9f60b9fda26", "User", "User" });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "bf3dbbef-a4ab-491a-a9d1-a8ed6f5bcf82", "a6378b06-ac06-4d45-8f39-8c75cf0a0080", "Public", "Public" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "8f797b11-8271-4cc7-9525-3a93ef87f06b");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "bf3dbbef-a4ab-491a-a9d1-a8ed6f5bcf82");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "c69caafa-1014-4423-9db9-fab7d376619e");
        }
    }
}
