﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AuthProject.Migrations
{
    public partial class AddingSubcategoriesTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "1bcae590-0b20-4eef-ab21-1deaa1acd0f7");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "9c7cd3a7-4e66-405b-a791-cfd1ba644e00");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "bbd5d67a-23ca-4b8b-a02e-3e0047efa529");

            migrationBuilder.AddColumn<string>(
                name: "RoleId",
                table: "Category",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "SubCategory",
                columns: table => new
                {
                    SubCategoryId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CategoryId = table.Column<int>(type: "int", nullable: false),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    RoleId = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubCategory", x => x.SubCategoryId);
                    table.ForeignKey(
                        name: "FK_SubCategory_Category_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "Category",
                        principalColumn: "CategoryId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "9bfde9dd-422a-40a0-a111-be16aefcfe18", "1bc474f8-f6bd-470e-a562-a003d9b4cbf6", "Admin", "Admin" });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "565f8d28-af8c-4e8d-ace6-3c9399c1b121", "e2627046-feb5-418b-947e-de1936005bf7", "User", "User" });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "46495f52-a086-4e28-8a3e-9619e5bf39fc", "c7a13a5a-6441-43d2-acf4-abcee1123a7d", "Public", "Public" });

            migrationBuilder.CreateIndex(
                name: "IX_SubCategory_CategoryId",
                table: "SubCategory",
                column: "CategoryId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SubCategory");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "46495f52-a086-4e28-8a3e-9619e5bf39fc");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "565f8d28-af8c-4e8d-ace6-3c9399c1b121");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "9bfde9dd-422a-40a0-a111-be16aefcfe18");

            migrationBuilder.DropColumn(
                name: "RoleId",
                table: "Category");

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "bbd5d67a-23ca-4b8b-a02e-3e0047efa529", "f36deb87-dc21-4c67-b7a0-9f2bf7d79d74", "Admin", "Admin" });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "9c7cd3a7-4e66-405b-a791-cfd1ba644e00", "9f5ee1e3-2f7a-43e8-95f0-4753689fdec5", "User", "User" });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "1bcae590-0b20-4eef-ab21-1deaa1acd0f7", "09a1d0da-7881-41f3-aa11-58be7f6dd93d", "Public", "Public" });
        }
    }
}
