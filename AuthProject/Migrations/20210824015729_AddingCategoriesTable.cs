﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AuthProject.Migrations
{
    public partial class AddingCategoriesTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "8f797b11-8271-4cc7-9525-3a93ef87f06b");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "bf3dbbef-a4ab-491a-a9d1-a8ed6f5bcf82");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "c69caafa-1014-4423-9db9-fab7d376619e");

            migrationBuilder.CreateTable(
                name: "Category",
                columns: table => new
                {
                    CategoryId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Category", x => x.CategoryId);
                });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "bbd5d67a-23ca-4b8b-a02e-3e0047efa529", "f36deb87-dc21-4c67-b7a0-9f2bf7d79d74", "Admin", "Admin" });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "9c7cd3a7-4e66-405b-a791-cfd1ba644e00", "9f5ee1e3-2f7a-43e8-95f0-4753689fdec5", "User", "User" });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "1bcae590-0b20-4eef-ab21-1deaa1acd0f7", "09a1d0da-7881-41f3-aa11-58be7f6dd93d", "Public", "Public" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Category");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "1bcae590-0b20-4eef-ab21-1deaa1acd0f7");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "9c7cd3a7-4e66-405b-a791-cfd1ba644e00");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "bbd5d67a-23ca-4b8b-a02e-3e0047efa529");

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "8f797b11-8271-4cc7-9525-3a93ef87f06b", "8ae811cc-c33b-4dbc-9f0d-a783ee7f015c", "Admin", "Admin" });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "c69caafa-1014-4423-9db9-fab7d376619e", "04335528-d854-429c-af0c-f9f60b9fda26", "User", "User" });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "bf3dbbef-a4ab-491a-a9d1-a8ed6f5bcf82", "a6378b06-ac06-4d45-8f39-8c75cf0a0080", "Public", "Public" });
        }
    }
}
