﻿using System;
using System.Collections.Generic;

namespace AuthProject.Models
{
    public class UserViewModel
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public string RoleId { get; set; }
        public List<RolesViewModel> Roles { get; set; }
    }
}
