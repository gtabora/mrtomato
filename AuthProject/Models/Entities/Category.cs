﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AuthProject.Models
{
    public class Category
    {
        [Key]
        public int CategoryId { get; set; }
        public string Description { get; set; }
        public string RoleId { get; set; }

        public sealed class Builder
        {
            private readonly Category _category;

            public Builder(string description, string role)
            {
                _category = new Category
                {
                    Description = description,
                    RoleId = role
                };
            }


            public Builder WithDescription(string description)
            {
                _category.Description = description;
                return this;
            }

            public Builder WithRole(string role)
            {
                _category.RoleId = role;
                return this;
            }

            public Category Build()
            {
                return _category;
            }
        }
    }
}
