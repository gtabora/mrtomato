﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AuthProject.Models
{
    public class SubCategory
    {
        [Key]
        public int SubCategoryId { get; set; }
        
        public int CategoryId { get; set; }
        public virtual Category Category { get; set; }

        public string Description { get; set; }
        public string RoleId { get; set; }

        public sealed class Builder
        {
            private readonly SubCategory _category;

            public Builder(string description, string role)
            {
                _category = new SubCategory
                {
                    Description = description,
                    RoleId = role
                };
            }


            public Builder WithDescription(string description)
            {
                _category.Description = description;
                return this;
            }

            public Builder WithRole(string role)
            {
                _category.RoleId = role;
                return this;
            }

            public SubCategory Build()
            {
                return _category;
            }
        }
    }

}
