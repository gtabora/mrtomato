﻿using System;
using System.Collections.Generic;

namespace AuthProject.Models
{
    public class CategoryViewModel
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public List<SubCategoryViewModel> SubCategories { get; set; }
        public List<RolesViewModel> Roles { get; set; }
        public string RolId { get; set; }
    }
}
