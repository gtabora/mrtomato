﻿using System;
namespace AuthProject.Models
{
    public class RolesViewModel
    {
        public string RolId { get; set; }
        public string Description { get; set; }
    }
}
