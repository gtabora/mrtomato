﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AuthProject.AppServices;
using AuthProject.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace AuthProject.Controllers
{
    [Authorize(Roles = "Admin")]
    public class CategoryController : Controller
    {
        private readonly CategoriesAppService _categoriesAppService;

        public CategoryController(CategoriesAppService categoriesAppService)
        {
            _categoriesAppService = categoriesAppService;
        }


        public IActionResult Index()
        {
            List<CategoryViewModel> categories = _categoriesAppService.GetCategories();
            
            return View(categories);
        }

        public ActionResult Create(int id)
        {
            CategoryViewModel category = _categoriesAppService.GetCategory(id);

            return View(category);
        }

        [HttpPost]
        public ActionResult Create(CategoryViewModel category)
        {
            _categoriesAppService.Save(category);

            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            _categoriesAppService.Delete(id);

            return RedirectToAction("Index");
        }
    }
}
