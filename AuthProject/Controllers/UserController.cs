﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AuthProject.AppServices;
using AuthProject.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace AuthProject.Controllers
{
    [Authorize(Roles = "Admin")]
    public class UserController : Controller
    {
        private readonly UserAppService _userAppService;

        public UserController(UserAppService userAppService)
        {
            _userAppService = userAppService;
        }


        public async Task<IActionResult> Index()
        {
            List<UserViewModel> users = await _userAppService.GetUsers();

            return View(users);
        }

        public async Task<ActionResult> Update(string id)
        {
            UserViewModel user = await _userAppService.GetUser(id);

            return View(user);
        }

        [HttpPost]
        public async Task<ActionResult> Update(UserViewModel user)
        {
            await _userAppService.Save(user);

            return RedirectToAction("Index");
        }
    }
}
